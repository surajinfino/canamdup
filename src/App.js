import React from "react";
import './App.css';
import Header from './components/Header/Header';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Footer from './components/Footer/Footer';
import Home from "./components/Pages/Home";
import About from "./components/Pages/About";
import Products from "./components/Products/Product";
import Contactus from "./components/Pages/Contactus";
import Singleproduct from "./components/Products/Singleproduct";
import Login from "./components/Pages/Login";
import Cart from "./components/Cart/Cart";
import Minicart from "./components/Cart/Minicart";
import Checkout from "./components/Checkout/Checkout";


export default function App() {
  return(
    <Router>
      <Header/>
      <Routes>
        <Route exact path='' element={<Home />} />
        <Route exact path='about' element={<About />}  />
        <Route exact path='products' element={<Products />} />
        <Route exact path='contact' element={<Contactus />} />
        <Route exact path="single-product" element={<Singleproduct />} />
        <Route exact path='login' element={<Login />} />
        <Route exact path='cart' element={<Cart />} />
        <Route exact path='minicart' element={<Minicart />} />
        <Route exact path='checkout' element={<Checkout />} />
      </Routes>
      <Footer/>
    </Router>
  );
}
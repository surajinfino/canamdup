import React, { useEffect, useState } from "react";
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import { Link } from 'react-router-dom';

function Singleproduct() {
  
  const[data, setData] = useState([])
  const id = window.location.search.split("=")[1];
  const url = 'https://fakestoreapi.com/products/'+id;
    useEffect( async ()=>{
        let result = await fetch(url);
        result = await result.json();
        setData(result)
    },[])
    console.log(data)
    
    return (
      <div>
          <section class="container py-12">
            <div className="md:flex">
              <div className="md:w-6/12 md:p-5 p-3 pb-8 mdd:w-9/12 mdd:m-auto">
                {<img className="m-auto" width="400px" height="100%" src={data.image} /> || <Skeleton count={1} width={500} />}
              </div>
              <div className="md:w-6/12 md:p-5 p-3">
                <p class="capitalize text-gray-900 md:text-xl title-font font-medium mb-3">{data.category || <Skeleton count={1} />}</p>
                <h2 className="container pb-7 text-xl md:text-2xl lg:text-4xl font-medium">{data.title || <Skeleton height={30} />}</h2>        
                <p class="text-gray-900 text-sm md:text-base title-font md:leading-7 leading-7 mb-2">{data.description || <Skeleton count={2} />}</p>
                <p class="text-gray-900 text-sm md:text-base title-font md:leading-7 leading-7 mb-8">{data.description || <Skeleton count={2} />}</p> 
                <h4 class="text-gray-900 md:text-4xl text-3xl title-font font-bold mb-4">$ {data.price || <Skeleton count={1} />}</h4>
                <Link to='/cart'><button className=" text-sm mt-7 xl:text-base font-medium bg-orange-500 hover:bg-[#214878] block py-4 p-8  text-white uppercase rounded-tl-2xl rounded-br-2xl focus:outline-none ">Add to Cart</button></Link>
              </div>
            </div>
          </section>
      </div>
    )
}

export default Singleproduct;



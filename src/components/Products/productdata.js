import Dummy from '../images/current.png'

const products = (
    {
      id : "1",
      title : "Nike Shoes",
      product_name : "Apple",
      description : "Screen Touch, Greatest Touch",
      price : "9301",
      currency : "$",
      thumb : Dummy
    }
)

export default products;
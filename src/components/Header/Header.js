import React, { useState, useEffect } from "react";
import logo from '../images/logo.png';
import { Link } from 'react-router-dom';

function Header() {
    const [isOpen, setIsOpen] = useState(false);    
    const [isClose] = useState(true);
    useEffect(() => {
      document.body.classList.toggle('modal-open', isOpen);
    },[isOpen])

    const [isOpens, setIsOpens] = useState(false);
    const toggling = () => setIsOpens(!isOpens);
  return ( 
     
  <header className="sticky top-0 z-50 bg-white text-gray-600 border-b border-gray-300">
    <div className="headerTop bg-[#214878]">
      <div className="container text-center md:ml-auto md:mr-auto py-2 px-2 text-white text-[13px] mdd:text-xs">
          <p className="">We will match any competitor's published shipping price to give you the lowest price, guaranteed. <span className="text-orange-500">"Details: Shipping by FedEx or Purolator"</span></p>
      </div> 
    </div>
    <div className="mdd:relative max-w-md:p2 2xl:pl-14 md:pl-4 px-3 full-width mx-auto flex flex-wrap justify-between flex-col md:flex-row items-center">
      <Link to={''} className="mdd:mb-0 py-1 xl:w-1/12 w-36 md:order-1 xl:order-none md:mr-auto md:pl-4 xl:pl-0 flex items-center mb-4 md:mb-0">
        <img src={logo} alt="logo" />
      </Link>
      <nav className="mdd:absolute mdd:left-2 mdd:top-8 xl:w-8/12 xl:ml-auto md:order-none xl:order-2 flex flex-wrap items-center text-base justify-center">
          <div className="hidden xl:flex items-center space-x-1 w-full">
            <ul className="font-semibold justify-evenly w-full flex flex-wrap text-base 2xl:text-lg">
            <li><Link to={'/about'} className="px-4 hover:text-[#f96f16]">Why CAN-AM</Link></li>
            <li><Link to={'/products'} className="px-4 hover:text-[#f96f16]">Sperm</Link></li>
            <li><Link to={'/single-product'} className="px-4 hover:text-[#f96f16]">Eggs</Link></li>
            <li><Link to={'/'} className="px-4 hover:text-[#f96f16]">Banking</Link></li>
            <li><Link to={'/'} className="px-4 hover:text-[#f96f16]">Fees</Link></li>
            <li><Link to={'/'} className="px-4 hover:text-[#f96f16]">Forms</Link></li>
            <li><Link to={'/'} className="px-4 hover:text-[#f96f16]">Brochures</Link></li>
            <li><span className="px-4 hover:text-[#f96f16] cursor-pointer" onClick={toggling}>Help Center <svg viewBox="0 0 32 32" class="icon icon-chevron-bottom" className="w-6 inline-block" viewBox="0 0 32 32" aria-hidden="true"><path d="M16.003 18.626l7.081-7.081L25 13.46l-8.997 8.998-9.003-9 1.917-1.916z"/></svg></span>
              {isOpens && (
                  <div className="divide-y divide-gray-200 py-1 text-base absolute bg-white top-full w-48 shadow-2xl ">
                  <li className="px-1 py-3"><Link to={'/'} className="block px-4 hover:text-[#f96f16]">Link0</Link></li>
                  <li className="px-1 py-3"><Link to={'/'} className="px-4 block hover:text-[#f96f16]">Link1</Link></li>
                  <li className="px-1 py-3"><Link to={'/'} className="px-4 block hover:text-toggling[#f96f16]">Link2</Link></li>
                  </div>
              )}
            </li>
            </ul>
          </div>
          <div className="xl:hidden flex items-center">
            <button className="outline-none menu-button" onClick={() => setIsOpen(!isOpen)}>
                <svg className="w-6 h-6 text-gray-500" x-show="! showMenu" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 00 24 24" stroke="currentColor"><path d="m4 6h16M4 12h16M4 18h16"></path></svg>
            </button>
          </div>
          <div className={"shadow-[0_35px_60px_-15px_rgba(0,0,0,0.3)] z-10 bg-white w-80 fixed space-y-6  inset-y-0 left-0 transform  transition duration-200 ease-in-out xl:hidden mobileMenu" + (isOpen ? " translate-x-0 transition-all  transition-all" : " hidden ")} >  
          <svg onClick={() => setIsOpen(!isClose)} class="svg-icon" viewBox="0 0 20 20" className="w-14 p-3 ml-auto">
                <path d="M10.185,1.417c-4.741,0-8.583,3.842-8.583,8.583c0,4.74,3.842,8.582,8.583,8.582S18.768,14.74,18.768,10C18.768,5.259,14.926,1.417,10.185,1.417 M10.185,17.68c-4.235,0-7.679-3.445-7.679-7.68c0-4.235,3.444-7.679,7.679-7.679S17.864,5.765,17.864,10C17.864,14.234,14.42,17.68,10.185,17.68 M10.824,10l2.842-2.844c0.178-0.176,0.178-0.46,0-0.637c-0.177-0.178-0.461-0.178-0.637,0l-2.844,2.841L7.341,6.52c-0.176-0.178-0.46-0.178-0.637,0c-0.178,0.176-0.178,0.461,0,0.637L9.546,10l-2.841,2.844c-0.178,0.176-0.178,0.461,0,0.637c0.178,0.178,0.459,0.178,0.637,0l2.844-2.841l2.844,2.841c0.178,0.178,0.459,0.178,0.637,0c0.178-0.176,0.178-0.461,0-0.637L10.824,10z"></path>
              </svg>
              <ul className="m-0 flex flex-col list-none lg:ml-auto justify-evenly font-medium text-lg ">
              <li className="border-b border-t border-gray-300"><Link to={'/about'} className="text-base px-5 block py-4 hover:text-[#f96f16]">Why CAN-AM</Link></li> 
              <li className="border-b border-gray-300"><Link to={'/'} className="text-base block py-4 px-5 hover:text-[#f96f16]">Sperm</Link></li>
              <li className="border-b border-gray-300"><Link to={'/'} className="text-base block py-4 px-5 hover:text-[#f96f16]">Eggs</Link></li>
              <li className="border-b border-gray-300"><Link to={'/'} className="text-base block py-4 px-5 hover:text-[#f96f16]">Banking</Link></li>
              <li className="border-b border-gray-300"><Link to={'/'} className="text-base block py-4 px-5 hover:text-[#f96f16]">Fees</Link></li>
              <li className="border-b border-gray-300"><Link to={'/'} className="text-base block py-4 px-5 hover:text-[#f96f16]">Forms</Link></li>
              <li className="border-b border-gray-300"><Link to={'/'} className="text-base block py-4 px-5 hover:text-[#f96f16]">Brochures</Link></li>
              <li className="border-b border-gray-300"><Link to={'/'} className="text-base block py-4 px-5 hover:text-[#f96f16]">Help Center</Link></li>
              </ul>
          </div>
      </nav>
      <div className="mdd:w-auto xl:w-3/12 w-96 inline-flex items-center flex-wrap md:order-2 xl:order-3">
        <div className="headerContact">
            <Link to={'/contact'} className="hidden md:block hover:bg-[#214878] inline-block py-4 p-8 bg-orange-500 text-white uppercase rounded-tl-2xl rounded-br-2xl" href="/">Contact Us</Link>
        </div>
        <ul className="flex flex-wrap text-center flex-auto justify-end">
          <li className="mdd:absolute mdd:p-0 mdd:left-12 mdd:top-8 mdd:border-none mdd:w-auto w-[30%] py-8 border-r-2 border-l-2"><Link to={'/login'} href="fa-phone"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 m-auto w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" /></svg></Link></li>
          <li className="mdd:absolute mdd:p-0 mdd:right-12 mdd:top-8 mdd:border-none mdd:w-auto w-[30%]  py-8 border-r-2"><Link to={'/minicart'}><svg xmlns="http://www.w3.org/2000/svg" className="h-6 m-auto w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" /></svg></Link></li>
          <li className="mdd:absolute mdd:p-0 mdd:right-2 mdd:top-8 mdd:border-none mdd:w-auto w-[30%]  h-full py-8">EN</li>
        </ul>
      </div>
    </div>
  </header>
  )
}
export default Header;

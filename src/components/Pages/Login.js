import React from 'react';

export default function login() {
const title = 'Customer Login';
  return <div>
      <div className="container">
        <h1 className="text-3xl md:text-5xl lg:text-7xl px-2 md:py-20 py-10 text-center font-semi bold text-indigo-400">{title}</h1>
        <h2 className="px-5 md:text-2xl text-xl md:text-4xl lg:text-5xl md:pb-8 font-semibold">Registered Customers</h2>
        <div className="container px-5 mx-auto flex md:flex-nowrap flex-wrap">
          <div className="w-full md:w-1/2 rounded-lg overflow-hidden lg:mr-16 md:mr-8 py-10 relative">
            <h3 className="border-b md:text-xl text-base pb-4 md:mb-10 mb-6 font-medium">Returning client</h3>
            <div className="md:mb-2">
            <label for="name" class="leading-7 text-sm text-gray-600 pb-2 block">Email *</label>
              <input type="text" id="name" name="name" placeholder="First Name"  className=" w-full mb-4 placeholder-gray-300 bg-white rounded border border-gray-300 focus:ring-1 focus:ring-indigo-300 text-base outline-none text-gray-700 py-1.5 px-5 leading-9 transition-colors duration-200 ease-in-out" />
            </div>
            <div className="mb-6">
              <label for="name" class="leading-7 text-sm text-gray-600 pb-2 block">Password *</label>
              <input type="text" id="name" name="name" placeholder="First Name"  className=" w-full mb-4 placeholder-gray-300 bg-white rounded border border-gray-300 focus:ring-1 focus:ring-indigo-300 text-base outline-none text-gray-700 py-1.5 px-5 leading-9 transition-colors duration-200 ease-in-out" />
            </div>
            <button className="md:block hover:bg-[#214878] inline-block py-4 p-8 bg-orange-500 text-white uppercase rounded-tl-2xl rounded-br-2xl">Sign in</button>
          </div>
          <div className="w-full md:w-1/2 rounded-lg overflow-hidden md:py-10 md:pl-6 py-10 mdd:pb-10 ">
            <h3 className="border-b text-xl pb-4 md:mb-10 mb-6 font-medium">New Customers</h3>
            <p className="pb-10 text-sm md:text-base">Creating an account has many benefits: check out faster, keep more than one address, track orders and more.</p>
            <button className="md:block hover:bg-[#214878] inline-block py-4 p-8 bg-orange-500 text-white uppercase rounded-tl-2xl rounded-br-2xl">Create an account</button>
          </div>
        </div>
    </div>

  </div>;
}

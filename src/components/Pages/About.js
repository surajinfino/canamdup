import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'



function About() {
    const page_title = 'Listing Page'
    const[data, setData] = useState([]);
    useEffect( async ()=>{
        let result = await fetch("https://fakestoreapi.com/products");
        result = await result.json();
        setData(result)
    },[]) 
    const listItems = data.map((item) =>
        <div className="card lg:px-4 md:px-2 px-1 lg:mb-8 md:mb-4 mb-2 w-1/2 md:w-1/3 lg:w-1/4" key={item.id}>
            <div className="inside border border-inherit md:p-6 p-3">
                <div className="card_img">
                    <Link to={'/single-product?pid='+item.id}><img className="m-auto object-contain" style={{height:320}} src={item.image} /></Link>
                </div>
                <div className="card_head">
                <Link to={'/products?pid='+item.id}><h2 className="md:h-16 h-14 overflow-hidden md:text-base text-sm md:font-semibold pt-4 font-medium">{item.title || <Skeleton variant="text" height={30} width={100} />}</h2></Link>
                <p>{item.count}</p>
                <p className="price text-2xl text-orange-500 font-bold pt-4">{item.price}</p>
                </div>
            </div>
        </div>
    );
    
    return(
        <div className="container px-3 py-10">
            <h2 className="text-3xl md:text-5xl lg:text-7xl px-2 md:py-10  text-center font-semi bold text-indigo-400">{page_title || <Skeleton height={30} />}</h2>
            <div className="my-8 md:-mx-2 -mx-1 mainList flex flex-wrap">
                {listItems}
            </div>
        </div>
    )

    // return (
    //     <div className="container">
    //         <h2 className="text-4xl font-semibold">{page_title}</h2>          
    //         {
    //             data.map((item)=>
    //                 <div className="card px-2 mb-10 w-1/2 md:w-1/3 lg:w-1/4 ">
    //                     <p>{item.id}</p>
    //                     <div className="card_img">
    //                         <img src={item.image} />
    //                     </div>
    //                 </div>
    //             )
    //         }            
    //     </div>
    // )
}

export default About



/* const About = () => {
    console.log(product_list);
    const listItems = product_list.map((item) =>
        <div className="card px-2 mb-10 w-1/2 md:w-1/3 lg:w-1/4" key={item.id}>
            <div className="card_img">
                <img src={item.thumb} />
            </div>
            <div className="card_head">
                <h2 className="text-xl font-semibold pt-4">{item.product_name}</h2>
                <p className="py-2">{item.description}</p>
                <p className="price text-xl text-black font-semibold">{item.currency}<span>{item.price}</span></p>
            </div>
        </div>
    );
    
    return(
        <div className="container px-3 py-10">
            <h2 className="text-4xl font-semibold">{page_title}</h2>
            <div className="my-10 -mx-2 mainList flex flex-wrap">
                {listItems}
            </div>
        </div>
    )
} */

// const page_title = 'Listing Page'
// // const About = () => {

// //     const [products, setProducts] = useState("");
    
// //     useEffect(() => {
// //         const url = "https://fakestoreapi.com/products";

// //         const fetchData = async () => {
// //             try {
// //                 const response = await fetch(url);
// //                 const json = await response.json();
// //                 setProducts(json);
// //             } catch (error) {
// //                 console.log("error", error);
// //             }
// //         };
// //         fetchData();
// //     }, []);

//     const listItems = products.map((item) => 

//         <div className="card px-2 mb-10 w-1/2 md:w-1/3 lg:w-1/4" key={item.id}>
//             <Link to={'/products?pid='+item.id}>
//                 <div className="card_img">
//                     <img src={item.image} />
//                 </div>
//                 <div className="card_head">
//                     <h2 className="text-xl font-semibold pt-4">{item.title}</h2>
//                     <p className="py-2">{item.description}</p>
//                     <p className="price text-xl text-black font-semibold">{item.price}<span>{item.price}</span></p>
//                 </div>
//             </Link>
//         </div>
//     );
    
//     return(
//         <div className="container px-3 py-10">
//             <h2 className="text-4xl font-semibold">{page_title}</h2>
//             <div className="my-10 -mx-2 mainList flex flex-wrap">
//                 {listItems}
//             </div>
//         </div>
//     )

// export default About;


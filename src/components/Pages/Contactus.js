import React from 'react';

export default function Contactus() {
const title = 'Get in Touch';
  return <div>
      <div className="container">
        <h2 className="text-3xl md:text-5xl lg:text-7xl px-2 md:pt-20 pt-10 text-center font-semi bold text-indigo-400">{title}</h2>
        <div className="container px-5 md:py-24 py-12 mx-auto flex md:flex-nowrap flex-wrap">
            <div className="w-full shadow-[0_0_20px_0_rgba(0,0,0,0.3)] shadow-gray-300 md:w-1/2 rounded-lg overflow-hidden lg:mr-10 md:mr-10 md:px-8 lg:px-12 px-6 lg:py-20 md:py-14 py-10 relative">
                <h2 className="text-2xl md:text-4xl lg:text-5xl md:pb-10 pb-8 font-semibold">Contact Us</h2>
                <div className="md:flex md:justify-between md:mb-2 md:-mx-2">
                    <input type="text" id="name" name="name" placeholder="First Name"  className="md:mx-2 w-full mb-4 placeholder-gray-300 md:w-2/4 bg-white rounded border border-gray-300 focus:ring-1 focus:ring-indigo-300 text-base outline-none text-gray-700 py-1.5 px-5 leading-9 transition-colors duration-200 ease-in-out" />
                    <input type="text" id="name" name="name" placeholder="Last Name"  className="md:mx-2 w-full mb-4 placeholder-gray-300 md:w-2/4 bg-white rounded border border-gray-300 focus:ring-1 focus:ring-indigo-300 text-base outline-none text-gray-700 py-1.5 px-5 leading-9 transition-colors duration-200 ease-in-out" />
                </div>
                <div className="md:flex justify-between md:mb-2 md:-mx-2">
                    <input type="text" id="name" name="name" placeholder="First Name"  className="md:mx-2 w-full mb-4 placeholder-gray-300 md:w-2/4 md:w-80 bg-white rounded border border-gray-300 focus:ring-1 focus:ring-indigo-300 text-base outline-none text-gray-700 py-1.5 px-5 leading-9 transition-colors duration-200 ease-in-out" />
                    <input type="text" id="name" name="name" placeholder="Last Name"  className="md:mx-2 w-full mb-4 placeholder-gray-300 md:w-80 md:w-2/4 bg-white rounded border border-gray-300 focus:ring-1 focus:ring-indigo-300 text-base outline-none text-gray-700 py-1.5 px-5 leading-9 transition-colors duration-200 ease-in-out" />
                </div>
                <div className="mb-4">
                    <textarea id="message" placeholder="Message" name="message" className="placeholder-gray-300 w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-indigo-200 h-44 text-base outline-none text-gray-700 py-6 px-5 mb-6 resize-none leading-6 transition-colors duration-200 ease-in-out"></textarea>
                </div>
                <button className="md:block hover:bg-[#214878] inline-block py-4 p-8 bg-orange-500 text-white uppercase rounded-tl-2xl rounded-br-2xl">Submit</button>
            </div>
            <div className="w-full md:w-1/2 rounded-lg overflow-hidden lg:px-12 lg:px-12  lg:py-20 md:py-4 py-10 mdd:pb-10">
                <h2 className="text-2xl md:text-4xl lg:text-5xl pb-8 md:pb-10 font-semibold">Hamilton Office</h2>
                <div className="relative mb-10 lg:mb-14">
                    <p className="flex items-center text-md lg:text-xl font-light md:leading-8"><svg className="fill-orange-500 mr-2 lg:mr-5" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-4.198 0-8 3.403-8 7.602 0 4.198 3.469 9.21 8 16.398 4.531-7.188 8-12.2 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.343-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg> 102-1057 Main Street West, <br/>Hamilton, ON.Canada L8S 1B7</p>
                </div>
                <div className="relative mb-8 pb-8 flex border-b-slate-300 border-b">
                    <p className="flex items-center lg:text-xl font-light mr-5 lg:mr-10"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className="fill-orange-500 mr-2 lg:mr-5"><path d="M20 22.621l-3.521-6.795c-.008.004-1.974.97-2.064 1.011-2.24 1.086-6.799-7.82-4.609-8.994l2.083-1.026-3.493-6.817-2.106 1.039c-7.202 3.755 4.233 25.982 11.6 22.615.121-.055 2.102-1.029 2.11-1.033z"/></svg> 905-524-3936</p>
                    <p className="flex items-center lg:text-xl font-light"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className="fill-orange-500 mr-2 lg:mr-5"><path d="M20 22.621l-3.521-6.795c-.008.004-1.974.97-2.064 1.011-2.24 1.086-6.799-7.82-4.609-8.994l2.083-1.026-3.493-6.817-2.106 1.039c-7.202 3.755 4.233 25.982 11.6 22.615.121-.055 2.102-1.029 2.11-1.033z"/></svg>905-524-3936</p>
                </div>
                <div className="relative mb-8 pb-8 flex border-b-slate-300 border-b">
                    <p className="flex items-center lg:text-xl font-light mr-5 lg:mr-10"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className="fill-orange-500 mr-2 lg:mr-5"><path d="M20 22.621l-3.521-6.795c-.008.004-1.974.97-2.064 1.011-2.24 1.086-6.799-7.82-4.609-8.994l2.083-1.026-3.493-6.817-2.106 1.039c-7.202 3.755 4.233 25.982 11.6 22.615.121-.055 2.102-1.029 2.11-1.033z"/></svg> 905-524-3936</p>
                    <p className="flex items-center lg:text-xl font-light"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className="fill-orange-500 mr-2 lg:mr-5"><path d="M20 22.621l-3.521-6.795c-.008.004-1.974.97-2.064 1.011-2.24 1.086-6.799-7.82-4.609-8.994l2.083-1.026-3.493-6.817-2.106 1.039c-7.202 3.755 4.233 25.982 11.6 22.615.121-.055 2.102-1.029 2.11-1.033z"/></svg> 905-524-3936</p>
                </div>
                <div className="relative mb-5 lg:flex">
                    <div className="text-lg font-light md:mr-10 mdd:mb-8 md:mb-8">
                        <h2 className="text-xl md:text-2xl lg:text-3xl md:pb-4 lg:pb-6 pb-5 font-medium">E-mail</h2>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24 " className="fill-orange-500 mr-4 float-left"><path d="M12 12.713l-11.985-9.713h23.97l-11.985 9.713zm0 2.574l-12-9.725v15.438h24v-15.438l-12 9.725z"/></svg><a href="mailto:info@canamcryo.com" className="text-indigo-500 underline">info@canamcryo.com</a></div>
                    <div className="lg:text-lg font-light">
                        <h2 className="text-xl lg:text-3xl md:pb-4 lg:pb-6 pb-5 md:text-2xl font-medium">Hours of Operation</h2>
                        <p>Monday to Friday 7.30am to 3.30pm EST</p>
                    </div>
                </div>
                <p className="lg:text-xl font-light">CAN-AM offers service in English</p>
            </div>
        </div>
    </div>

  </div>;
}
import React from "react";
import Banner from '../Slider/Slider';
import Donar from '../images/donar.png';
import Registration from '../images/registration-form.png';
import Order from '../images/order.png';
import Baby from '../images/baby.png';
import Expert from '../images/expert.png'
import Babypic from '../images/babypic.png'
import Aphrodite from '../images/aphrodite.png'
import Asianegg from '../images/asianegg.png'
import Cryos from '../images/cryos.png'
import Fairfax from '../images/fairfax.png'
import Fairaxegg from '../images/fairfaxegg.png'
import Nwcry from '../images/nwcry.png'
import Ovobank from '../images/ovobank.png'
import Seattle from '../images/seattle.png'
import Xytex from '../images/xytex.png'


export default function Home () {
    return (
        <div className="Main">
            <Banner/>
            <section className="3Step">
                <div className="container px-5 md:py-24 py-14 mx-auto">
                <h3 className="text-center text-2xl md:mb-11 mb-10 font-semibold md:text-4xl xl:text-5xl">3 Easy Steps to Order</h3>
                    <div className="flex flex-wrap -m-4">
                    <div className="p-2 lg:p-4 md:w-1/3">
                        <div className="p-5 lg:p-8 h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                        <img className="" src={Donar} alt="blog" />
                        <h1 className="title-font text-xl lg:text-3xl font-medium mt-7 lg:mt-9 mb-5 lg:mb-7">Choose A Donor</h1>
                        <p className="font-light leading-relaxed mb-3">Search through donors, quickly and easily. Filter by physical characteristics and find the right match for you.</p>
                        </div>
                    </div>
                    <div className="p-2 lg:p-4 md:w-1/3">
                        <div className="p-5 lg:p-8 h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                        <img className=" " src={Registration} alt="blog" />
                        <h1 className="title-font text-xl lg:text-3xl font-medium mt-7 lg:mt-9 mb-5 lg:mb-7">Create Your Profile</h1>
                        <p className="font-light leading-relaxed mb-3">Create your personalized profile and view your order options and progress.</p>
                        </div>
                    </div>
                    <div className="p-2 lg:p-4 md:w-1/3">
                        <div className="p-5 lg:p-8 h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                        <img className=" " src={Order} alt="blog" />
                        <h1 className="title-font text-xl lg:text-3xl font-medium mt-7 lg:mt-9 mb-5 lg:mb-7">Order</h1>
                        <p className="font-light leading-relaxed mb-3">Complete your purchase online and we will ship to your physician or store at CAN-AM.</p>
                        </div>
                    </div> 
                    </div>
                </div>
            </section>
            <section className="relative before:bg-gray-50 before:absolute before:inline-block before:z-50 before:inset-x-0 xl:before:top-20 xl:before:h-5/6 before:-z-10 before:h-full">
                <div className="pt-10 full-container mx-auto flex px-5 py-4 md:flex-row flex-col items-center">
                    <div className="lg:w-5/12 md:w-1/2 w-full mb-10 md:mb-0">
                        <img className="" src={Baby} alt="baby" />
                    </div>
                    <div className="lg:flex-grow md:w-1/2 xl:pl-24 md:pl-8 flex-col md:items-start md:text-left items-center">
                    <h2 className="text-2xl after:block after:mt-4 md:after:mt-6 after:bg-orange-500 after:h-0.5 after:w-28 xl:text-5xl md:text-4xl title-font md:mb-12 mb-8 max-w-screen-sm font-semibold">Only The Highest Standards</h2>
                    <p className="lg:pr-11 font-light xl:text-xl xl:w-6/12 lg:float-left mb-8 leading-relaxed">CAN-AM Cryoservices is the largest and most trusted distributor of donor sperm and eggs across Canada. Centrally-located in Hamilton, Ontario and with a close relationship with your physician/clinic, we can offer clients the best price and selection.</p>
                    <p className="font-light xl:w-6/12 lg:text-base lg:float-left mb-8 leading-relaxed">Our online donor search tool helps you find the donor that fits the criteria most important to you. Physical appearance, ethnic background, and donor type (Open Identity or non-open Identity) are all options within your control. If you would like some help to make your decision, we provide free donor consultations. 
                    <button className="mt-8 hover:bg-[#214878] block py-4 p-8 bg-orange-500 text-white uppercase rounded-tl-2xl rounded-br-2xl focus:outline-none ">Read more</button>
                    </p>
                    </div>
                </div>
            </section>

            <section className="">
                <div className="xl:py-20 mx-auto">
                    <div className="flex flex-wrap text-center">
                        <div className="w-full md:w-2/4">
                            <div className="relative overflow-hidden h-72 md:h-96 xl:h-[575px] flex-col flex justify-center items-center bg-cover bg-right" style={{backgroundImage: `url(${Expert})`}} >
                                <h2 className="max-w-screen-sm title-font text-2xl md:text-3xl xl:text-5xl font-semibold mb-2 xl:mb-5">#Ask The Experts</h2>
                                <button className="text-sm mt-7 xl:text-base font-medium hover:bg-[#214878] block py-4 p-8 bg-orange-500 text-white uppercase rounded-tl-2xl rounded-br-2xl focus:outline-none ">Read more</button>  
                                <p className="absolute md:right-5 bottom-3 text-base xl:text-2xl">With Quality Sperm & Eggs</p>
                            </div>
                        </div>
                        <div className="w-full md:w-2/4">
                            <div className="relative overflow-hidden h-72 md:h-96 xl:h-[575px] flex-col flex justify-center items-center bg-cover bg-left" style={{ backgroundImage: `url(${Babypic})` }}>
                                <h2 className="max-w-screen-sm text-white text-2xl title-font md:text-3xl xl:text-5xl font-semibold mb-2 xl:mb-5">Simple Answers to Difficult Questions</h2>
                                <button className="text-sm mt-7 xl:text-base font-medium hover:bg-[#214878] block py-4 p-8 bg-orange-500 text-white uppercase rounded-tl-2xl rounded-br-2xl focus:outline-none ">Read more</button>  
                                <p className="text-white absolute md:left-5 bottom-3 text-base xl:text-2xl">You can Build Your Family</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="">
                <div className="container px-3 py-14 xl:p-0 mx-auto">
                    <h3 className="text-center text-2xl md:mb-12 mb-8 font-semibold md:text-4xl xl:text-5xl">Registered Canadian Distributor For</h3>
                    <div className="flex flex-wrap mdd:-m-2 justify-center">
                        <div className="w-1/2 md:w-1/6 md:w-1/2 md:mb-5 p-2 w-full">
                            <a className="border border-gray-200 px-2.5 py-4 block rounded">
                            <img alt="ecommerce" className="object-cover object-center block m-auto" src={Aphrodite} />
                            </a>
                        </div>
                        <div className="w-1/2 md:w-1/6 md:w-1/2 p-2 md:mb-5 w-full">
                            <a className="border border-gray-200 px-2.5 py-4 block rounded">
                            <img alt="ecommerce" className="object-cover object-center block m-auto" src={Asianegg} />
                            </a>                            
                        </div>
                        <div className="w-1/2 md:w-1/6 md:w-1/2 p-2 md:mb-5 w-full">
                            <a className="border border-gray-200 px-2.5 py-4 block rounded">
                            <img alt="ecommerce" className="object-cover object-center block m-auto" src={Cryos} />
                            </a>
                        </div>
                        <div className="w-1/2 md:w-1/6 md:w-1/2 p-2 md:mb-5 w-full">
                            <a className="border border-gray-200 px-2.5 py-4 block rounded">
                            <img alt="ecommerce" className="object-cover object-center block m-auto" src={Fairfax} />
                            </a>
                        </div>
                        <div className="w-1/2 md:w-1/6 md:w-1/2 p-2 md:mb-5 w-full">
                            <a className="border border-gray-200 px-2.5 py-4  block rounded">
                            <img alt="ecommerce" className="object-cover object-center block m-auto" src={Fairaxegg} />
                            </a>
                        </div>
                        <div className="w-1/2 md:w-1/6 md:w-1/2 p-2 md:mb-5 w-full">
                            <a className="border border-gray-200 px-2.5 py-4 block rounded">
                            <img alt="ecommerce" className="object-cover object-center block m-auto" src={Nwcry} />
                            </a>
                        </div>
                        <div className="w-1/2 md:w-1/6 md:w-1/2 p-2 md:mb-5 w-full">
                            <a className="border border-gray-200 px-2.5 py-4 block rounded">
                            <img alt="ecommerce" className="object-cover object-center block m-auto" src={Ovobank} />
                            </a>
                        </div>
                        <div className="w-1/2 md:w-1/6 md:w-1/2 p-2 md:mb-5 w-full">
                            <a className="border border-gray-200 px-2.5 py-4 block rounded">
                            <img alt="ecommerce" className="object-cover object-center block m-auto" src={Seattle} />
                            </a>
                        </div>
                        <div className="w-1/2 md:w-1/6 md:w-1/2 p-2 md:mb-5 w-full">
                            <a className="border border-gray-200 px-2.5 py-4 block rounded">
                            <img alt="ecommerce" className="object-cover object-center block m-auto" src={Xytex} />
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div>    
    )
}

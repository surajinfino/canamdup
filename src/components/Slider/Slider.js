import React from "react";
import Banner from '../images/banner2.png';
import Slider from 'react-animated-slider';
import "react-animated-slider/build/horizontal.css";

export default function App() {
    return (
        <Slider autoplay={3000}>
            <div className="" style={{ background: `url('${Banner}') no-repeat top center` }}>
                <div className="container px-2 md-px-4">
                    <div className="center text-left absolute bottom-16">
                        <p className="md:text-xl text-base text-[#919191] font-light">Starting your journey to parenthood? <br/> We’re with you every baby-step of the way.</p>
                        <h2 className="md:text-7xl text-4xl text-[#4989da] pt-5 md:pt-8 pb-10 md:pb-12">Love <br/>Makes A Family</h2>
                        <button className="font-medium hover:bg-[#214878] rounded-tl-2xl rounded-br-2xl py-5  px-6 bg-orange-500 text-white ">FIND A SPERM DONOR</button>
                    </div>
                </div>
            </div> 
            <div className="" style={{ background: `url('${Banner}') no-repeat top center` }}>
                <div className="container px-2 md:px-4">
                <div className="text-left absolute bottom-16">
                        <p className="md:text-xl text-base text-[#919191] font-light">Starting your journey to parenthood? <br/> We’re with you every baby-step of the way.</p>
                        <h2 className="md:text-7xl text-4xl text-[#4989da] pt-5 md:pt-8 pb-10 md:pb-12">Love <br/>Makes A Family</h2>
                        <button className="font-medium hover:bg-[#214878] rounded-tl-2xl rounded-br-2xl py-5  px-6 bg-orange-500 text-white ">FIND A SPERM DONOR</button>
                    </div>
                </div>
            </div>        
        </Slider>
    )
}

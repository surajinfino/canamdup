import React from "react";
import ftrlogo from '../images/logo.png';
import paymenticon from '../images/payment.png'
import FtrBackground from '../images/background.png'

function Footer() {
    return (
        <footer className="full-container xl:pt-16 pt-10">
            <div className="h-[350px] md:h-[296px] bg-center flex  bg-no-repeat bg-cover" style={{backgroundImage: `url(${FtrBackground})`}}>
            <div className="flex container">
                <div className="lg:max-w-screen-lg md:max-w-screen-md ml-auto flex flex-wrap py-4 px-4 items-center justify-between w-full">
                    <div className="max-w-[300px] text-white">
                        <h3 className="text-2xl md:text-3xl lg:mb-8 mb-5">Get in Touch.</h3>
                        <p className="text-sm md:text-base">Are you interested in a consultation with one of our specialists?</p>
                    </div>
                    <div className="">
                        <div className="flex items-center">
                            <span className="bg-white p-4 rounded-full mr-3"><svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M8.26 1.289l-1.564.772c-5.793 3.02 2.798 20.944 9.31 20.944.46 0 .904-.094 1.317-.284l1.542-.755-2.898-5.594-1.54.754c-.181.087-.384.134-.597.134-2.561 0-6.841-8.204-4.241-9.596l1.546-.763-2.875-5.612zm7.746 22.711c-5.68 0-12.221-11.114-12.221-17.832 0-2.419.833-4.146 2.457-4.992l2.382-1.176 3.857 7.347-2.437 1.201c-1.439.772 2.409 8.424 3.956 7.68l2.399-1.179 3.816 7.36s-2.36 1.162-2.476 1.215c-.547.251-1.129.376-1.733.376"/></svg></span>
                            <strong className="text-white text-2xl md:text-3xl"><span className="block text-base font-normal mb-1 md:text-xl">Call us Now!</span>1-888-245-3471</strong>
                        </div>
                        <button className="text-sm mt-7 xl:text-base font-medium bg-[#214878] block py-4 p-8  text-white uppercase rounded-tl-2xl rounded-br-2xl focus:outline-none ">Contact Us</button>
                    </div>
                </div>
            </div> 
            </div>
            <div className="container px-2 py-12 md:py-18 lg:py-24 mx-auto flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
            <div className="flex-grow flex flex-wrap -mb-10 md:mt-0 mt-10 text-left">
                <div className="lg:w-1/4 md:w-1/2 w-full px-2">
                    <img className="m-auto md:ml-0 mb-6 md:mb-16 max-w-[220px]" src={ftrlogo} alt="logo" />
                    <p className="mt-2 text-center mb-10 md:text-left md:pr-16 leading-7 md:text-base text-sm">Copywrite © 2021 CAN-AM Cryoservices. All Rights Reserved.</p>
                </div>
                <div className="lg:w-1/4 md:w-1/2 w-1/2 px-2">
                    <h2 className="md:mb-6 mb-5 md:text-2xl text-xl font-medium">Quick Links</h2>
                    <nav className="list-none mb-10 ">
                    <li className="pb-3">
                        <a className="md:text-base text-sm ">Why CAN-AM</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm ">Sperm</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm ">Eggs</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm">Banking</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm">Fees</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm">Forms</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm">Brochures</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm">Help Center</a>
                    </li>
                    </nav>
                </div>
                <div className="lg:w-1/4 md:w-1/2 w-1/2 px-2">
                    <h2 className="md:text-2xl text-xl font-medium md:mb-6 mb-5">About</h2>
                    <nav className="list-none mb-10">
                    <li className="pb-3">
                        <a className="md:text-base text-sm ">About CAN-AM</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm">Québec clients</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm">Privacy policy</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm">Contact Us</a>
                    </li>
                    <li className="pb-3">
                        <a className="md:text-base text-sm">Terms & conditions</a>
                    </li>
                    </nav>
                </div>
                <div className="lg:w-1/4 md:w-1/2 w-full px-2">
                    <h2 className="md:text-2xl text-xl font-medium md:mb-6 mb-4">Follow Us</h2>
                    <span className="inline-flex sm:ml-auto sm:mt-0 mt-2 justify-center sm:justify-start">
                    <a className="border-2 p-3.5 rounded-full border-slate-300 border-solid text-gray-500">
                    <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-5 h-5" viewBox="0 0 24 24">
                        <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                    </svg>
                    </a>
                    <a className="border-2 p-3.5 rounded-full border-slate-300 border-solid  ml-3">
                    <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-5 h-5" viewBox="0 0 24 24">
                        <path d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z"></path>
                    </svg>
                    </a>
                    <a className="border-2 p-3.5 rounded-full border-slate-300 border-solid  ml-3 text-gray-500">
                    <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-5 h-5" viewBox="0 0 24 24">
                        <rect width="20" height="20" x="2" y="2" rx="5" ry="5"></rect>
                        <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
                    </svg>
                    </a>
                    <a className="border-2 p-3.5 rounded-full border-slate-300 border-solid  ml-3 text-gray-500">
                    <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="0" className="w-5 h-5" viewBox="0 0 24 24">
                        <path stroke="none" d="M16 8a6 6 0 016 6v7h-4v-7a2 2 0 00-2-2 2 2 0 00-2 2v7h-4v-7a6 6 0 016-6zM2 9h4v12H2z"></path>
                        <circle cx="4" cy="4" r="2" stroke="none"></circle>
                    </svg>
                    </a>
                    </span>
                    <img className="md:mt-16 mt-6" src={paymenticon} alt="" />
                </div>
            </div>
            </div>
        </footer>
    )
}

export default Footer;
import React, { useState } from "react";
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker';

export default function Checkout() {
const title = 'Checkout';
const [startDate, setStartDate] = useState(new Date());
  return (
    <div>
        <div className="container">
            <h1 className="text-3xl md:text-5xl lg:text-7xl px-2 md:py-20 py-10 text-center font-semi bold text-indigo-400">{title}</h1>
            <div className="container px-5 mx-auto">
                <p className='pb-6 text-xl font-light'>Fields with an asterisk (*) are required to complete you order.</p>
                <p className='pb-6 text-xl font-light'>If you have questions about this form, please call us at 905-5243342 or 1-888-245-3471 or e-mail us at info@canamcryo.com</p>
                <p className='pb-6 text-xl font-light'>CAN-AM cannot guarantee availability of Donor Sperm by fac, email or online order. Please call to confirm availability and receipt of your order. Information submitted by order form becomes property of CAN-AM.</p>
                <p className='pb-6 text-xl font-light'>National carriers like UPS and FedEX do not deliver on weekends, or on Federal or Provincial Statutory holidays.</p>
                <h3 className='py-6 text-3xl font-medium'>Please Note:</h3>
                <p className='pb-6 text-xl font-light'>One or more sperm banks may use the same donor number. Please verify that you have selected the correct donor bank before placing your order.</p>
            </div>
            <div className="container px-5 mx-auto flex lg:flex-nowrap flex-wrap">
              <div className="w-full lg:w-9/12 rounded-lg overflow-hidden lg:mr-10 md:mr-8 pb-10 relative">
                <div className=''>
                  <h3 className='py-6 text-3xl font-medium'>Shipping Information</h3>
                </div>
                <div className=''>
                  <h3 className='py-6 pb-8 text-3xl font-medium'>Shipping Address</h3>
                  <div className='relative lg:flex lg:flex-wrap'>
                    <select className='w-full md:w-2/5 md:mr-5 mb-5 py-4 px-5 bg-white rounded border border-gray-300 rounded border text-base'>
                      <option>Province</option>
                      <option>Province1</option>
                      <option>Province2</option>
                      <option>Province3</option>
                    </select> 
                    <select className='w-full md:w-2/5 mb-5 py-4 px-5 bg-white rounded border border-gray-300 rounded border text-base'>
                      <option>City</option>
                      <option>Canada</option>
                      <option>USA</option>
                      <option>India</option>
                    </select> 
                  </div>
                  <div className='relative lg:flex lg:flex-wrap'>
                    <select className='w-full md:w-2/5 md:mr-5 mb-5 py-4 px-5 bg-white rounded border border-gray-300 rounded border text-base'>
                      <option>Clinic</option>
                      <option>Clinic1</option>
                      <option>Clinic2</option>
                      <option>Clinic3</option>
                    </select> 
                    <select className='w-full md:w-2/5 mb-5 py-4 px-5 bg-white rounded border border-gray-300 rounded border text-base'>
                      <option>Other</option>
                      <option>Canada</option>
                      <option>USA</option>
                      <option>India</option>
                    </select> 
                  </div>
                  <div className='relative lg:flex lg:flex-wrap'>
                    <select className='w-full md:w-2/5 md:mr-5 mb-5 py-4 px-5 bg-white rounded border border-gray-300 rounded border text-base'>
                      <option>Physician's name</option>
                      <option>Province1</option>
                      <option>Province2</option>
                      <option>Province3</option>
                    </select> 
                    <select className='w-full md:w-2/5 mb-5 py-4 px-5 bg-white rounded border border-gray-300 rounded border text-base'>
                      <option>Other</option>
                      <option>Canada</option>
                      <option>USA</option>
                      <option>India</option>
                    </select> 
                  </div>
                  <DatePicker selected={startDate} onChange={(date:Date) => setStartDate(date)} />
                  
                </div>
                
              </div>
              <div className="w-full p-6 border-solid border lg:w-3/12 overflow-hidden border-neutral-300 py-6 relative">
                123
              </div>
              
            </div>
        </div>
    </div>
  )
}

import React from 'react'
import { Link } from 'react-router-dom';

export default function Cart() {
const title = 'Shopping Cart';
  return (
    <div>
        <div className="container">
            <h1 className="text-3xl md:text-5xl lg:text-7xl px-2 md:py-20 py-10 text-center font-semi bold text-indigo-400">{title}</h1>
            <div className="container px-5 mx-auto flex lg:flex-nowrap flex-wrap">
                <div className="w-full lg:w-9/12 rounded-lg overflow-hidden lg:mr-10 md:mr-8 pb-10 relative">
                    <div className='overflow-auto'>
                    <table className='w-full'>
                        <thead>
                            <tr className='border-solid border-b border-neutral-300 text-sm lg:text-base'>
                                <th className='lg:p-4 p-2 text-center'>Doner ID</th>
                                <th className='lg:p-4 p-2 text-center'>Sperm Bank</th>
                                <th className='lg:p-4 p-2 text-center'>Doner Type</th>
                                <th className='lg:p-4 p-2 text-center'>Height</th>
                                <th className='lg:p-4 p-2 text-center'>Weight</th>
                                <th className='lg:p-4 p-2 text-center'>Blood Type</th>
                                <th className='lg:p-4 p-2 text-center'>ICI / IUI</th>
                                <th className='lg:p-4 p-2 text-center'>#Of Vials</th>
                                <th className='lg:p-4 p-2 text-center'>Order Preference</th>
                            </tr>
                        </thead>
                        <tbody className='border-solid border-b border-neutral-300 text-sm lg:text-base'>
                            <tr>
                                <td className='lg:p-4 p-2 py-4 text-center'>5183</td>
                                <td className='lg:p-4 p-2 py-4 text-center'>Fairfax Cryobank</td>
                                <td className='lg:p-4 p-2 py-4 text-center'>Open ID</td>
                                <td className='lg:p-4 p-2 py-4 text-center'>6'0"</td>
                                <td className='lg:p-4 p-2 py-4 text-center'>137</td>
                                <td className='lg:p-4 p-2 py-4 text-center'>B+</td>
                                <td className='lg:p-4 p-2 py-4 text-center'>
                                    <select className='bg-transparent border-solid border border-neutral-300 p-2'>
                                        <option value="select">Select</option>
                                        <option value="number">Number</option>
                                        <option value="price">Price</option>
                                        <option value="rate">Rate</option>
                                    </select>
                                </td>
                                <td className='lg:p-4 p-2 py-4 text-center'>
                                    <select className='bg-transparent border-solid border border-neutral-300 p-2'>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </td>
                                <td className='lg:p-4 p-2 py-4 text-center'>5183</td>
                            </tr>
                            <tr>
                                <td colspan="100" className='text-right py-3'>
                                    <Link className='inline-block pr-4 text-base' to=""><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M7.127 22.562l-7.127 1.438 1.438-7.128 5.689 5.69zm1.414-1.414l11.228-11.225-5.69-5.692-11.227 11.227 5.689 5.69zm9.768-21.148l-2.816 2.817 5.691 5.691 2.816-2.819-5.691-5.689z"/></svg></Link>
                                    <Link className='inline-block text-base' to=""><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24"><path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/></svg></Link>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <p className='text-indigo-400 text-left underline font-medium text-sm pt-4'><Link to=''>Choose another donor</Link></p>
                </div>
                <div className="w-full p-6 border-solid border lg:w-3/12 overflow-hidden border-neutral-300 py-6 relative">
                    <h3 className='border-b text-base pb-6 mb-4 border-neutral-300 font-semibold'>Summary</h3>   
                    <h3 className='border-b text-sm pb-4 border-neutral-300 mb-6 font-semibold'>Subtotal Before Shipping & Taxes</h3>
                    <table className='table w-full'>
                        <tr className='text-sm'>
                            <td className='py-1'>Subtotal</td>
                            <td className='text-right py-1'>$2760.00</td>
                        </tr>
                        <tr className='text-sm'>
                            <td className='py-1'>Discount</td>
                            <td className='py-1 text-right'>$0.00</td>
                        </tr>
                        <tr className='text-sm border-b pb-6 clear-both'>
                            <td className='py-1 pb-5'>Tax</td>
                            <td className='py-1 pb-5 text-right'>$0.00</td>
                        </tr>
                        <tr>
                            <td className='pt-5 font-semibold'>Order Total</td>
                            <td className='pt-5 font-semibold text-right'>$2760.00</td>
                        </tr>
                    </table>
                    <Link to="/checkout"><button className="w-full text-sm mt-7 xl:text-base font-medium bg-orange-500 hover:bg-[#214878] block py-4 p-8  text-white uppercase rounded-tl-2xl rounded-br-2xl focus:outline-none ">Go to Checkout</button></Link>
                    <p className='text-indigo-400 text-center font-medium text-sm pt-4'><Link to=''>Check Out with Multiple Addresses</Link></p>
                </div>
            </div>
        </div>
    </div>
  )
}

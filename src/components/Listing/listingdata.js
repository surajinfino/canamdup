import Dummy from '../images/current.png'

const product_list = [
    {
        id : "1",
        product_name : "Mobile",
        description : "Screen Touch, Hand Free",
        price : "1010",
        currency : "$",
        thumb : Dummy
    },
    {
        id : "2",
        product_name : "Samung",
        description : "Screen Touch, Loudspeaker",
        price : "1510",
        currency : "$",
        thumb : Dummy,
        shortDesc : "hello"
    },
    {
        id : "3",
        product_name : "Apple",
        description : "Screen Touch, Greatest Touch",
        price : "1930",
        currency : "$",
        thumb : Dummy
    },
    {
        id : "4",
        product_name : "Donar Bank",
        description : "Screen Touch, Greatest Touch",
        price : "930",
        currency : "$",
        thumb : Dummy
    },
    {
        id : "5",
        product_name : "Sperm Bank",
        description : "Screen Touch, Greatest Touch",
        price : "530",
        currency : "$",
        thumb : Dummy
    },
    {
        id : "6",
        product_name : "Donar Bank",
        description : "Screen Touch, Greatest Touch",
        price : "30",
        currency : "$",
        thumb : Dummy
    }
]

export default product_list;

module.exports = {
    content: [
      "./src/**/*.{js,jsx,ts,tsx}",
    ],
    theme: {
      extend: {
        screens: {
          'mdd': {'max': '767px'},
        },
      },
    },
    plugins: [],
  }